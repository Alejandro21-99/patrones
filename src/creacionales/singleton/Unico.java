package creacionales.singleton;

public class Unico {

    private static Unico unico;

    private Unico(){}

    public synchronized static Unico getUnico(){

        if (unico == null){
            unico = new Unico();
        }
        return unico;

    }



}
