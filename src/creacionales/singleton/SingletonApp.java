package creacionales.singleton;

public class SingletonApp {

    public static void main(String[] args) {

        System.out.println("Se cumple el patron con diferentes hilos gracias a 'sincronized'");

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                Unico unico1 = Unico.getUnico();
                System.out.println("Primer Hilo: " + unico1.hashCode());
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                Unico unico2 = Unico.getUnico();
                System.out.println("Segundo Hilo: " + unico2.hashCode());
            }
        });

        t1.start();
        t2.start();

    }

}
