package creacionales.prototype;

public interface CuentaClonar extends Cloneable{

    CuentaClonar clonar()  throws CloneNotSupportedException;

}
