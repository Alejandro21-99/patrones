package creacionales.prototype;

public class Cuenta implements CuentaClonar{

    private String tipo;
    private double saldo;


    public void cargarValores(){
        tipo = "Cuenta de Ahorro";
        saldo = 10000;
    }


    @Override
    public CuentaClonar clonar()  throws CloneNotSupportedException{
        Cuenta cuenta = (Cuenta) clone();
        return cuenta;
    }

    public String getTipo() {
        return tipo;
    }

    public void cambiarTipo(String tipo) {
        this.tipo = tipo;
    }

    public double getSaldo() {
        return saldo;
    }

    public void restarSaldo(double saldo) {
        this.saldo = this.saldo - saldo;
    }

    @Override
    public String toString() {
        return "Cuenta{" +
                "tipo='" + tipo + '\'' +
                ", saldo=" + saldo +
                '}';
    }
}
