package creacionales.prototype;

public class PrototypeApp {

    public static void main(String[] args) throws CloneNotSupportedException {

        Cuenta cuentaOrginial = new Cuenta();
        cuentaOrginial.cargarValores();
        Cuenta cuentaClonada = (Cuenta) cuentaOrginial.clonar();
        Cuenta cuentaClonada2 = (Cuenta) cuentaOrginial.clonar();

        System.out.println("Cuenta Original despues de cargar valores: "+cuentaOrginial);

        cuentaClonada.restarSaldo(2500);
        System.out.println("Cuenta Clonada despues de restar 2500: "+cuentaClonada);

        cuentaClonada2.cambiarTipo("Cuenta Corriente");
        System.out.println("Cuenta Clonada 2 despues de cambiar tipo de cuenta: "+cuentaClonada2);

    }

}
