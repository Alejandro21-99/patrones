package creacionales.abstract_factory;

public class AbstractFactoryApp {

    public static void main(String[] args) {

        ComidaFactory comidaFactory;

        System.out.print("Preparamos (Fabricamos) Comida Italiana\n");
        comidaFactory = new ComidaItalianaFactory();
        comidaFactory.preparar().almuerzo();
        comidaFactory.preparar().cena();

        System.out.print("\n");
        System.out.print("Preparamos(Fabricamos) Comida Nicaraguense\n");
        comidaFactory = new ComidaNiraguenseFactory();
        comidaFactory.preparar().cena();
        comidaFactory.preparar().almuerzo();

    }

}
