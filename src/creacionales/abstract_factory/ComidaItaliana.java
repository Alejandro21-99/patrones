package creacionales.abstract_factory;

public class ComidaItaliana implements Comida{
    @Override
    public void almuerzo() {
        System.out.print("Preparando Almuerzo Italiano: Pizza\n");
    }

    @Override
    public void cena() {
        System.out.print("Preparando Cena Italiana: Pasta\n");
    }
}
