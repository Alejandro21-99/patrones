package creacionales.abstract_factory;

public interface ComidaFactory {

    Comida preparar();

}
