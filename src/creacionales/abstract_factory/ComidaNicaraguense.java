package creacionales.abstract_factory;

public class ComidaNicaraguense implements Comida{


    @Override
    public void almuerzo() {
        System.out.print("Preparando Almuerzo Nica: Vigoron\n");
    }

    @Override
    public void cena() {
        System.out.print("Preparando Cena Nica: Fritanga\n");
    }
}
