package creacionales.abstract_factory;

public interface Comida {

    void almuerzo();

    void cena();

}
