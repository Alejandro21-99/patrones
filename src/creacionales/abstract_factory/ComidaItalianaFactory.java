package creacionales.abstract_factory;

public class ComidaItalianaFactory implements ComidaFactory{

    @Override
    public Comida preparar() {
        return new ComidaItaliana();
    }
}
