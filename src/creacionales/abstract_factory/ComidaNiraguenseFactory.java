package creacionales.abstract_factory;

public class ComidaNiraguenseFactory implements ComidaFactory{
    @Override
    public Comida preparar() {
        return new ComidaNicaraguense();
    }
}
