package creacionales.builder;

public class VehiculoBuilder {

    private String marca;
    private String modelo;

    private boolean esMecanico;

    public VehiculoBuilder(String marca, String modelo){
        this.marca = marca;
        this.modelo = modelo;
    }

    public VehiculoBuilder setEsMecanico(boolean esMecanico) {
        this.esMecanico = esMecanico;
        return this;
    }

    public boolean isEsMecanico() {
        return esMecanico;
    }

    public Vehiculo build(){
        return new Vehiculo(this);
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

}
