package creacionales.builder;

public class BuilderApp {

    public static void main(String[] args) {

        Vehiculo vehiculo = new VehiculoBuilder("Hyundai", "Accent").setEsMecanico(true).build();

        System.out.println(vehiculo);

    }

}
