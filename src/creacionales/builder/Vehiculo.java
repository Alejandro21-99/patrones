package creacionales.builder;

public class Vehiculo {

    private String marca;
    private String modelo;

    private boolean esMecanico;

    public String getMarca() {
        return marca;
    }

    public String getModelo() {
        return modelo;
    }

    public boolean isEsMecanico() {
        return esMecanico;
    }

    public Vehiculo(VehiculoBuilder vehiculoBuilder) {
        this.marca = vehiculoBuilder.getMarca();
        this.modelo = vehiculoBuilder.getModelo();
        this.esMecanico = vehiculoBuilder.isEsMecanico();
    }

    @Override
    public String toString() {
        return "Vehiculo{" +
                "marca='" + marca + '\'' +
                ", modelo='" + modelo + '\'' +
                ", esMecanico=" + esMecanico +
                '}';
    }

}
