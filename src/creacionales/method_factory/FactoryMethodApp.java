package creacionales.method_factory;

public class FactoryMethodApp {
    static Compra compra;
    static int tipo = 1;

    public static void main(String...ars){

        if(tipo == 1) compra = new EfectivoCreador();
        else if(tipo == 2) compra = new TarjetaDeCreditoCreador();

        compra.comprarAlgo();
    }
}
