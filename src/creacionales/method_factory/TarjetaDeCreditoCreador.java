package creacionales.method_factory;

public class TarjetaDeCreditoCreador extends Compra{


    @Override
    Pago hacerPago() {
        return new TarjetaDeCredito();
    }
}
