package creacionales.method_factory;

public class TarjetaDeCredito implements Pago {
    @Override
    public void metodoDePago() {
        System.out.println("Se compró algo con Tajeta de Credito");
    }
}
