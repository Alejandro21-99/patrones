package creacionales.method_factory;

public class EfectivoCreador extends Compra{


    @Override
    Pago hacerPago() {
        return new Efectivo();
    }
}
