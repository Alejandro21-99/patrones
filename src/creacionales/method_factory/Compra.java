package creacionales.method_factory;

public abstract class Compra {

    public void comprarAlgo(){
        Pago pago = hacerPago();
        pago.metodoDePago();
    }

    abstract Pago hacerPago();

}
